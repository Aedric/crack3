#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings
const int STEPSIZE=100;       // Memory increase for realloc
int dict_count=0;
// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5

    // Compare the two hashes

    // Free any malloc'd memory

    return 0;
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    FILE *f = fopen( filename, "r");
    if (!f)
        {
        printf("read_hashes: file error message\n");
        exit(1);
        }
    
    int arrlen = 0;    
    
    char **lines = NULL;
    char buf[1000];
    int i = 0;
    while (fgets(buf, 1000, f) != NULL)
        {
        if (i == arrlen)
            {
            arrlen += STEPSIZE;
            char **newlines = realloc(lines, arrlen * sizeof(char *));
            if (!newlines)
                {
                printf( "read_hashes: newlines error message\n");
                exit(1);
                }
            lines = newlines;
            }// end of if
            
        buf[strlen(buf) - 1] = '\0';
        lines[i] = strdup(buf);
        i++;
        }// end of while
    
    if (i == arrlen)
        {
        char **newarr = realloc(lines, (arrlen+1) * sizeof(char *));
        if (!newarr)
            {
            printf("read_hashes: newarr error message\n");
            exit(1);
            }
        lines = newarr;
        }
    
    lines[i] = NULL;
    return lines;
}// end of read_hashes


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    if (!f)
        {
        printf("read_dict: file error message\n");
        exit(1);
        }
    
    int arrlen = 0;
    char **dict = NULL;
    char buf[PASS_LEN];
    while (fgets(buf, PASS_LEN, f) != NULL)
        {
        if (dict_count == arrlen)
            {
            arrlen += STEPSIZE;
            char **newdict = realloc(dict, arrlen * sizeof(char*));
            if (!newdict)
                {
                printf("read_dict: newdict error message\n");
                exit(1);
                }
            dict = newdict;
            }// end of if
        
        buf[strlen(buf) - 1] = '\0';
        char *str = strdup(buf);
        
        char output[(HASH_LEN + PASS_LEN + 2)];
        sprintf(output, "%s:%s", md5(str, strlen(str)), str);
        
        dict[dict_count] = strdup(output);
        dict_count++;
        }// end of while
    
    if (dict_count == arrlen)
        {
        char **newarr = realloc(dict, (arrlen + 1) * sizeof(char*));
        if (!newarr)
            {
            printf("read_dict: newarr error message\n");
            exit(1);
            }
        dict = newarr;
        }
    
    dict[dict_count] = NULL;
    return dict;
}// end of read_dict

int qcompare( const void *a, const void *b)
    {
    return strncmp( *((char **)a), *((char **)b), HASH_LEN);
    }
    
int bcompare( const void *key, const void *elem )
    {
    return strncmp( (char *)key, *((char **)elem), HASH_LEN - 1);
    }

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes( argv[1] );

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict( argv[2] );
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dict_count, sizeof(char *), qcompare);
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
   
    for( int i = 0; hashes[i] != NULL; i++)
        {
        char **found = bsearch( hashes[i], dict, dict_count, sizeof(char *), bcompare);
        
        if (found != NULL)
            {
            printf("%s\n", *found);
            }
        }
    //for (int i = 0; hashes[i] != NULL; i++)
        //printf("%d: %s\n", i, hashes[i]);
    //for (int i = 0; dict[i] != NULL; i++)
      //  printf("%d: %s\n", i, dict[i] );
    
    
    free(hashes);
    free(dict);
}
